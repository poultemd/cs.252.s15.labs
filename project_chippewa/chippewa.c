/* chippewa: Simple HTTP Server */

#include "chippewa.h"

#include <errno.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>

/* Global Variables */
char *Port		    = "9898";
char *MimeTypesPath	    = "/etc/mime.types";
char *DefaultMimeType	    = "text/plain";
char *RootPath		    = ".";
mode  ConcurrencyMode	    = SINGLE;
pthread_mutex_t PopenMutex  = /* TODO */

/**
 * Display usage message.
 */
static void
usage(const char *progname)
{
    fprintf(stderr, "usage: %s [hcmMpr]\n", progname);
    fprintf(stderr, "options:\n");
    fprintf(stderr, "    -h            Display help message\n");
    fprintf(stderr, "    -c mode       Single, Forking, or Threaded mode\n");
    fprintf(stderr, "    -m path       Path to mimetypes file\n");
    fprintf(stderr, "    -M mimetype   Default mimetype\n");
    fprintf(stderr, "    -p port       Port to listen on\n");
    fprintf(stderr, "    -r path       Root directory\n");
}

/**
 * Parses command line options and starts appropriate server
 */
int
main(int argc, char *argv[])
{
    int c;
    int sfd;

    /* Parse command line options */
    /* TODO */

    /* Listen to server socket */
    /* TODO */

    /* Determine real RootPath */
    /* TODO */

    log("Listening on port %s", Port);
    debug("RootPath        = %s", RootPath);
    debug("MimeTypesPath   = %s", MimeTypesPath);
    debug("DefaultMimeType = %s", DefaultMimeType);
    debug("ConcurrencyMode = %s", ConcurrencyMode == 0 ? "Single" : ConcurrencyMode == 1 ? "Forking" : "Threaded");

    /* Start server with specified concurrency mode */
    /* TODO */

    return (EXIT_SUCCESS);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
