/* handler.c: HTTP Request Handlers */

#include "chippewa.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <unistd.h>

/* Internal Declarations */
http_status handle_browse_request(struct request *request);
http_status handle_file_request(struct request *request);
http_status handle_cgi_request(struct request *request);
http_status handle_error(struct request *request, http_status status);

/**
 * Handle HTTP Request
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
http_status
handle_request(struct request *r)
{
    http_status result;

    /* Parse request */
    /* TODO */

    /* Determine request path */
    /* TODO */

    /* Dispatch to appropriate request handler type */
    /* TODO */

    log("HTTP REQUEST STATUS: %s", http_status_string(result));
    return (result);
}

/**
 * Handle browse request
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
http_status
handle_browse_request(struct request *r)
{
    struct dirent **entries;
    int n;

    /* Open a directory for reading or scanning */
    /* TODO */

    /* Write HTTP Header with OK Status and text/html Content-Type */
    /* TODO */

    /* For each entry in directory, emit HTML list item */
    /* TODO */

    /* Flush socket, return OK */
    /* TODO */

    return (HTTP_STATUS_OK);
}

/**
 * Handle file request
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
http_status
handle_file_request(struct request *r)
{
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
    /* TODO */

    /* Determine mimetype */
    /* TODO */

    /* Write HTTP Headers with OK status and determined Content-Type */
    /* TODO */

    /* Read from file and write to socket in chunks */
    /* TODO */

    /* Close file, flush socket, deallocate mimetype, return OK */
    /* TODO */

    return (HTTP_STATUS_OK);

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    /* TODO */
    return (HTTP_STATUS_INTERNAL_SERVER_ERROR);
}

/**
 * Handle file request
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
http_status
handle_cgi_request(struct request *r)
{
    FILE *pfs;
    char buffer[BUFSIZ];

    /* With PopenMutex */
        /* Export CGI environment variables from request:
         * http://en.wikipedia.org/wiki/Common_Gateway_Interface */
        /* TODO */

        /* Export CGI environment variables from request headers */
        /* TODO */

        /* Popen CGI Script */
        /* TODO */

    /* Check popen */
    /* TODO */

    /* Copy data from popen to socket */
    /* TODO */

    /* Close popen, flush socket, return OK */
    /* TODO */

    return (HTTP_STATUS_OK);
}

/**
 * Handle displaying error page
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
http_status
handle_error(struct request *r, http_status status)
{
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */
    /* TODO */

    /* Write HTML Description of Error*/
    /* TODO */

    return (status);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
