/* forking.c: Forking HTTP Server */

#include "chippewa.h"

#include <errno.h>
#include <string.h>

#include <pthread.h>
#include <unistd.h>

/**
 * Handle single connection
 **/
void *
handle_threaded_request(void *arg)
{
    /* Handle request */
    /* TODO */

    /* Free request */
    /* TODO */

    /* Exit thread */
    /* TODO */
}

/**
 * Spawn threads for incoming HTTP requests to handle them concurrently
 *
 * The parent should accept a request and then spawn a thread to handle the
 * request.
 **/
void
threaded_server(int sfd)
{
    struct request *request;
    pthread_t thread;

    /* Accept and handle HTTP request */
    while (1) {
    	/* Accept request */
    	/* TODO */

	/* Spawn and detach child to handle request */
    	/* TODO */
    }

    /* Close server socket and exit */
    /* TODO */
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
