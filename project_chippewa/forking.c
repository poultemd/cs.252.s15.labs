/* forking.c: Forking HTTP Server */

#include "chippewa.h"

#include <errno.h>
#include <signal.h>
#include <string.h>

#include <unistd.h>

/**
 * Forking version of HTTP server that utilizes a process per connection.
 *
 * The parent process accepts an incoming HTTP request and then forks a child
 * to handle the request.
 **/
void
forking_server(int sfd)
{
    struct request *request;
    pid_t pid;

    /* Accept and handle HTTP request */
    while (1) {
    	/* Accept request */
	/* TODO */

	/* Ignore children */
	/* TODO */

	/* Fork off child process to handle request */
	/* TODO */
    }

    /* Close server socket and exit */
    /* TODO */
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
